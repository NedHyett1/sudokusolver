using System.Collections.Generic;
using System.Linq;

namespace SudokuSolver.Models
{
    public class Sector : ISolvable
    {
     
        public (int, int) Position { get; }
        public int Factor { get; }
        public Board ParentBoard { get; }
        public Cell[,] Cells { get; }
        
        public Sector(Board parent, (int, int) position, int factor)
        {
            ParentBoard = parent;
            Position = position;
            Factor = factor;
            Cells = new Cell[factor,factor];
            
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
            {
                var pos = (Position.Item1 * factor + x, Position.Item2 * factor + y);
                Cells[x, y] = new Cell(this, pos, factor);
            }
        }

        /// <summary>
        /// Reset all Cells in this Sector to empty.
        /// </summary>
        public void Reset()
        {
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                Cells[x, y].Reset();
        }
        
        /// <summary>
        /// Checks how many Cells in this Sector are capable of containing the number.
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public int CountNumberOfPossibleCellsForNumber(int number)
        {
            Helpers.IsValidUserlandNumber(number);
            var count = 0;
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                if (Cells[x, y].PossibleNumbers[number - 1] && !Cells[x,y].Number.HasValue)
                    count++;

            return count;
        }

        private int[] GetRemainingNumbers()
        {
            var ret = new List<int>();

            for (var i = 1; i <= Factor*Factor; i++)
                if (CountNumberOfPossibleCellsForNumber(i) > 0)
                    ret.Add(i);

            return ret.ToArray();
        }

        private Cell[] GetCellsForPossibleNumber(int number)
        {
            var ret = new List<Cell>();
            
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
            {
                var cell = Cells[x, y];
                if(cell.IsSolved())
                    continue;

                if (cell.PossibleNumbers[number - 1])
                    ret.Add(cell);
            }

            return ret.ToArray();
        }

        public bool IsNumberSet(int number)
        {
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                if (Cells[x, y].Number.HasValue && Cells[x, y].Number == number)
                    return true;

            return false;
        }

        public bool IsSolved()
        {
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                if (!Cells[x, y].IsSolved())
                    return false;

            return true;
        }

        public void CalculatePossibleNumbers()
        {
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                Cells[x, y].CalculatePossibleNumbers();
        }

        public void Solve()
        {
            for (var x = 0; x < Cells.GetLength(0); x++)
            for (var y = 0; y < Cells.GetLength(1); y++)
                Cells[x, y].Solve();

            var rNumbers = GetRemainingNumbers();

            foreach (var rNumber in rNumbers)
            {
                /**
                 * For this method to work, all instances of the possible Housing Cells for this number must be in a
                 * Sub-Line. A simple first check for this is that the number of remaining Cells must be less than
                 * or equal to the Factor of the Board (i.e. the Sector dimensions). This ensures that there are
                 * only enough remaining numbers to potentially fill a Sub-Line in this Sector.
                 *
                 * Our goal here is that if a Sector has a number where the possible position only appears
                 * in one Row or Column inside a Sector (otherwise a "Sub-Line"), then all other possible instances
                 * of that number in the same Row or Column in other Sectors must be discounted in order to solve
                 * the puzzle, the possible number MUST appear in that Sector in that Sub-Line, so we can predict
                 * that it must be in that Row or Column with 100% certainty, eliminating unknowns in other Sectors.
                 *
                 * As an example, given two Sectors that look like, and are arranged in the Board
                 * as shown below with one beneath the other:
                 *
                 * A 2 4
                 * 8 7 B
                 * 1 6 C
                 *
                 * 4 8 D
                 * E F 2
                 * G H 6
                 *
                 * Where:
                 * - A could possibly house 3, 9
                 * - B could possibly house 3, 5
                 * - C could possibly house 5, 9
                 * - D is unknown (but it is known it cannot house 3 due to an unseen adjacent Sector)
                 * - E could possibly house 3
                 * - F could possibly house 1 (and cannot house 3)
                 * - G could possibly house 3
                 * - H could possibly house 1 (and cannot house 3)
                 *
                 * As E and G both could house 3 and the Cell above E is known (with value 4), then the two
                 * possible instances of 3 form a Sub-Line that indicate that:
                 *
                 * 1) the number 3 MUST be in that Line.
                 * 2) the number 3 MUST be in that Line in that Sector in order to satisfy the conditions of the Sector.
                 *
                 * Therefore we can reasonably determine that Cell A can no longer house 3 and that the value of
                 * A must now in fact be 9.
                 * 
                 */
                
                if (CountNumberOfPossibleCellsForNumber(rNumber) <= Factor)
                {
                    var cells = GetCellsForPossibleNumber(rNumber);

                    if (cells.Same(c => c.Position.Item2)) //cells for row
                    {
                        var row = ParentBoard.GetRow(cells[0].Position.Item2);
                        foreach (var cell in row)
                        {
                            if (cell.ParentSector != this)
                            {
                                cell.PossibleNumbers[rNumber - 1] = false;
                            }
                        }
                    }
                    else if (cells.Same(c => c.Position.Item1)) //cells for col
                    {
                        var col = ParentBoard.GetColumn(cells[0].Position.Item1);
                        
                        foreach (var cell in col)
                        {
                            if (cell.ParentSector != this)
                            {
                                cell.PossibleNumbers[rNumber - 1] = false;
                            }
                        }
                    }
                }
                
            }
        }
    }
}