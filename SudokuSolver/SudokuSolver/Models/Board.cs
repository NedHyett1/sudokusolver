using System;
using System.Linq;

namespace SudokuSolver.Models
{
    public class Board
    {
     
        public int Factor { get; }
        public Sector[,] Sectors { get; }
        public bool AutomaticMode { get; private set; } = false;

        public Board(int factor = 3)
        {
            Factor = factor;
            Sectors = new Sector[factor,factor];
            for (var x = 0; x < Sectors.GetLength(0); x++)
            for (var y = 0; y < Sectors.GetLength(1); y++)
                Sectors[x, y] = new Sector(this, (x, y), factor);
        }

        

        /// <summary>
        /// Directly set the value of a Cell. Set the Cell to null to clear it. 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="value"></param>
        public void SetCell(int x, int y, int? value)
        {
            var cell = GetCellAtCoordinate(x, y);
            cell.Number = value < 0 ? null : value;
        }

        /// <summary>
        /// Get the Cell at the specified coordinates.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public Cell GetCellAtCoordinate(int x, int y)
        {
            for (var secX = 0; secX < 3; secX++)
            for (var secY = 0; secY < 3; secY++)
            {
                var sec = Sectors[secX, secY];
                for (var inSecX = 0; inSecX < 3; inSecX++)
                for (var inSecY = 0; inSecY < 3; inSecY++)
                {
                    var cell = sec.Cells[inSecX, inSecY];
                    if (cell.Position.Item1 == x && cell.Position.Item2 == y)
                        return cell;
                }
            }

            return null;
        }

        /// <summary>
        /// Get all Cells in the specified row (horizontal)
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public Cell[] GetRow(int row)
        {
            var ret = new Cell[Sectors.GetLength(0) * Sectors.GetLength(0)];
            for (var x = 0; x < ret.Length; x++)
                ret[x] = GetCellAtCoordinate(x, row);

            return ret;
        }
        
        /// <summary>
        /// Get all Cells in the specified column (vertical)
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public Cell[] GetColumn(int column)
        {
            var ret = new Cell[Sectors.GetLength(1) * Sectors.GetLength(1)];
            for (var y = 0; y < ret.Length; y++)
                ret[y] = GetCellAtCoordinate(column, y);

            return ret;
        }

        /// <summary>
        /// Draw the Board to the stdout.
        /// </summary>
        public void Draw()
        {
            for (var y = -1; y < Factor*Factor; y++)
            {
                if (y > -1)
                    Console.Write((y + 1) + " ");
                else
                    Console.Write("  ");

                for (var x = 0; x < Factor * Factor; x++)
                {
                    if (y == -1)
                    {
                        Console.Write($"{((x + 1) % Factor == 0 && x != ((Factor*Factor) - 1) && x != 0 ? (x+1+"   ") : x+1+" ")}");
                        continue;
                    }
                    var box = GetCellAtCoordinate(x, y);
                    Console.Write((box.Number.HasValue ? box.Number.Value.ToString() : " ") + " " + ((x + 1) % Factor == 0 && x != ((Factor*Factor) - 1) && x != 0 ? ("| ") : ""));
                }
                
                Console.Write("\n");
                
                if ((y + 1) % Factor != 0 || y == ((Factor*Factor) - 1)) continue;
                Console.WriteLine("  ------+-------+------");
            }
        }

        /// <summary>
        /// Count the number of Cells in the specified row that are currently marked as being capable of holding the specified number.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="pNum"></param>
        /// <returns></returns>
        public int CountNumberOfPossibleCellsForNumberInRow(int row, int pNum)
        {
            var rowBoxes = GetRow(row);
            return rowBoxes.Count(box => box.PossibleNumbers[pNum - 1]);
        }

        /// <summary>
        /// Count the number of Cells in the specified column that are currently marked as being capable of holding the specified number.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="pNum"></param>
        /// <returns></returns>
        public int CountNumberOfPossibleCellsForNumberInColumn(int col, int pNum)
        {
            var colBoxes = GetColumn(col);
            return colBoxes.Count(box => box.PossibleNumbers[pNum - 1]);
        }

        public bool IsSolved()
        {
            for (var x = 0; x < Sectors.GetLength(0); x++)
            for (var y = 0; y < Sectors.GetLength(1); y++)
                if (!Sectors[x, y].IsSolved())
                    return false;

            return true;
        }

        public void Solve()
        {
            AutomaticMode = true;
            
            for (var x = 0; x < Sectors.GetLength(0); x++)
            for (var y = 0; y < Sectors.GetLength(1); y++)
                Sectors[x, y].CalculatePossibleNumbers();
            
            for (var x = 0; x < Sectors.GetLength(0); x++)
            for (var y = 0; y < Sectors.GetLength(1); y++)
                Sectors[x, y].Solve();
            
            //Naked Pair
            for (var y = 0; y < Factor * Factor; y++)
            {
                var row = GetRow(y);
                SolveNakedPairs(row);
            }

            for (var x = 0; x < Factor * Factor; x++)
            {
                var col = GetColumn(x);
                SolveNakedPairs(col);
            }
            

            AutomaticMode = false;
        }

        private void SolveNakedPairs(Cell[] line)
        {
            foreach (var cell in line)
            {
                if (cell.IsSolved() || cell.NumberOfRemainingPossibleNumbers > 2)
                    continue;
                    
                foreach (var cell1 in line)
                {
                    if (cell1.IsSolved())
                        continue;
                        
                    if (cell != cell1 && cell1.NumberOfRemainingPossibleNumbers == 2)
                    {
                        var valid = true;
                        for(var num = 0; num < cell.PossibleNumbers.Length; num++)
                        {
                            if (cell.PossibleNumbers[num] != cell1.PossibleNumbers[num])
                            {
                                valid = false;
                                break;
                            }
                        }

                        if (!valid)
                            continue;
                            
                        //is valid naked pair
                        
                        Console.WriteLine($"Naked pair at: ({cell.Position.Item1 + 1}, {cell.Position.Item2 + 1}), ({cell1.Position.Item1 + 1}, {cell1.Position.Item2 + 1})");

                        foreach (var cell2 in line)
                        {
                            if (cell2.IsSolved())
                                continue;
                                
                            if (cell2 == cell || cell2 == cell1) 
                                continue;
                                
                            for (var i = 0; i < cell.PossibleNumbers.Length; i++)
                            {
                                if (cell.PossibleNumbers[i])
                                {
                                    cell2.PossibleNumbers[i] = false;
                                }
                            }
                        }

                        if (cell.ParentSector == cell1.ParentSector)
                        {
                            for(var x = 0; x < cell.ParentSector.Cells.GetLength(0); x++)
                            for (var y = 0; y < cell.ParentSector.Cells.GetLength(1); y++)
                            {
                                var cell2 = cell.ParentSector.Cells[x, y];
                                if (cell2.IsSolved())
                                    continue;
                                
                                if (cell2 == cell || cell2 == cell1) 
                                    continue;
                                
                                for (var i = 0; i < cell.PossibleNumbers.Length; i++)
                                {
                                    if (cell.PossibleNumbers[i])
                                    {
                                        cell2.PossibleNumbers[i] = false;
                                    }
                                }
                            }
                        }
                            
                    }
                }
            }
        }
        
        /// <summary>
        /// Reset all Sectors to empty.
        /// </summary>
        public void Reset()
        {
            for (var x = 0; x < Sectors.GetLength(0); x++)
            for (var y = 0; y < Sectors.GetLength(1); y++)
                Sectors[x, y].Reset();
        }
        
    }
}