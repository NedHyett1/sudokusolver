using System;
using System.Collections.Generic;
using System.Linq;

namespace SudokuSolver.Models
{
    public class Cell : ISolvable
    {

        public (int, int) Position { get; }
        public int Factor { get; }
        public bool[] PossibleNumbers { get; }

        public int NumberOfRemainingPossibleNumbers => PossibleNumbers.Count(a => a);
        public Sector ParentSector { get; }

        public Cell[] Row => ParentSector.ParentBoard.GetRow(Position.Item2);
        public Cell[] Col => ParentSector.ParentBoard.GetColumn(Position.Item1);

        private int? _number;
        public int? Number
        {
            get => _number;
            set
            {
                if (value != null)
                {
                    Helpers.IsValidUserlandNumber(value.Value);
                    for (var x = 0; x < PossibleNumbers.Length; x++)
                        PossibleNumbers[x] = false;

                    if (ParentSector.ParentBoard.AutomaticMode)
                    {
                        var board = ParentSector.ParentBoard;
                        var row = board.GetRow(Position.Item2);
                        var col = board.GetColumn(Position.Item1);

                        foreach (var cell in row)
                        {
                            if(cell == this)
                                continue;

                            if (!cell.IsSolved())
                            {
                                cell.PossibleNumbers[value.Value - 1] = false;
                            }
                        }

                        foreach (var cell in col)
                        {
                            if (cell == this)
                                continue;
                        
                            if (!cell.IsSolved())
                            {
                                cell.PossibleNumbers[value.Value - 1] = false;
                            }
                        }

                        for (var x = 0; x < ParentSector.Cells.GetLength(0); x++)
                        for (var y = 0; y < ParentSector.Cells.GetLength(1); y++)
                        {
                            var cell = ParentSector.Cells[x, y];

                            if (cell == this)
                                continue;

                            if (!cell.IsSolved())
                            {
                                cell.PossibleNumbers[value.Value - 1] = false;
                            }
                        }
                    }
                }

                _number = value;
            }
        }

        
        public Cell(Sector parent, (int, int) position, int factor)
        {
            ParentSector = parent;
            Position = position;
            Factor = factor;
            PossibleNumbers = new bool[factor*factor];
            Reset();
        }

        private int[] GetRemainingPossibleNumbers()
        {
            var ret = new List<int>();

            for (var i = 1; i <= PossibleNumbers.Length; i++)
                if (PossibleNumbers[i - 1])
                    ret.Add(i);

            return ret.ToArray();
        }
        
        public bool IsNumberSet(int number)
        {
            return Number.HasValue && Number.Value == number;
        }

        public bool IsSolved()
        {
            return Number.HasValue;
        }

        public void CalculatePossibleNumbers()
        {
            if (IsSolved())
                return;

            //Calc the row
            var row = Row;
            for (var num = 1; num <= PossibleNumbers.Length; num++)
                if (row.Any(b => b.Number.HasValue && b.Number.Value == num))
                    PossibleNumbers[num - 1] = false;

            //Calc the col
            var col = Col;
            for (var num = 1; num <= PossibleNumbers.Length; num++)
                if (col.Any(b => b.Number.HasValue && b.Number.Value == num))
                    PossibleNumbers[num - 1] = false;

            //Calc the sector
            for (var num = 1; num <= PossibleNumbers.Length; num++)
                if (ParentSector.IsNumberSet(num))
                    PossibleNumbers[num - 1] = false;
            
        }

        public void Solve()
        {
            if (IsSolved())
                return;

            var pNumbers = GetRemainingPossibleNumbers();

            //If there is only one remaining possible number we already have an answer.
            if (pNumbers.Length == 1)
            {
                Number = pNumbers[0];
                return;
            }
            
            //For each remaining possible number...
            foreach (var pNumber in pNumbers)
            {
                //If this number can only be placed in this cell in this sector...
                if (ParentSector.CountNumberOfPossibleCellsForNumber(pNumber) <= 1)
                {
                    Number = pNumber;
                    return;
                }
                
                //If this number can only be placed in this cell in this row
                if (ParentSector.ParentBoard.CountNumberOfPossibleCellsForNumberInRow(Position.Item2, pNumber) <= 1)
                {
                    Number = pNumber;
                    return;
                }
                
                //If this number can only be placed in this cell in this column
                if (ParentSector.ParentBoard.CountNumberOfPossibleCellsForNumberInColumn(Position.Item1, pNumber) <= 1)
                {
                    Number = pNumber;
                    return;
                }
            }
            
            
            
            if(!PossibleNumbers.Any() && !Number.HasValue)
                throw new Exception($"Invalid state in box {Position.Item1+1},{Position.Item2+1}");
            
        }

        public void CalculateChangeChain(int forNumber)
        {
            //If this cell was set to this number, what would the subsequent chain of events be?
            
            //TODO: be arsed to do this.
            
        }

        public void Reset()
        {
            Number = null;
            for (var x = 0; x < PossibleNumbers.Length; x++)
                PossibleNumbers[x] = true;
        }
    }
}