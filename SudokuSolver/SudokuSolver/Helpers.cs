using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace SudokuSolver
{
    public static class Helpers
    {

        public static void IsValidUserlandNumber(int number)
        {
            if(!(number >= 1 && number <= 9))
                throw new Exception($"Value '{number}' is not within range.");
        }

        public static bool Same<TEnumerable, TPropertyValue>(this IEnumerable<TEnumerable> enumerable, Func<TEnumerable, TPropertyValue> func)
        {
            if(enumerable == null || !enumerable.Any())
                throw new Exception("Empty set");

            var firstVal = func(enumerable.First());

            return enumerable.All(t => Equals(func(t), firstVal));
        }
        
    }
}