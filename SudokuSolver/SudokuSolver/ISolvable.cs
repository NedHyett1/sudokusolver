namespace SudokuSolver
{
    public interface ISolvable
    {

        bool IsNumberSet(int number);
        
        /// <summary>
        /// Does this unit of solvability consider itself "solved"?
        /// </summary>
        /// <returns></returns>
        bool IsSolved();
        
        /// <summary>
        /// Calculate the possible numbers within this unit.
        /// </summary>
        void CalculatePossibleNumbers();
        
        /// <summary>
        /// Calculate, or try to calculate the correct value using methods appropriate to this unit of solvability.
        /// </summary>
        void Solve();

        /// <summary>
        /// Reset this unit back to the default state.
        /// </summary>
        void Reset();

    }
}