﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using SudokuSolver.Models;

namespace SudokuSolver
{
    internal static class Program
    {
        private static Board _board;
        private static bool _showAllSteps = false;
        
        public static void Main(string[] args)
        {
            _board = new Board();
            
            
            while (true)
            {
                _board.Draw();
                var line = GetInput();
                switch (line)
                {
                    case "quit":
                    case "Quit":
                        return;
                    
                    case "solve":
                    case "Solve":
                        Solve();
                        break;
                    
                    case "clear":
                        _board.Reset();
                        break;
                    
                    case "iter":
                        _board.Solve();
                        break;

                    case "opt-sas":
                    {
                        _showAllSteps = !_showAllSteps;
                        Console.WriteLine($"Show working? {_showAllSteps}");
                        break;
                    }

                    case string cmddbg when cmddbg.StartsWith("dbg-cell "):
                    {
                        var dbg = cmddbg.Substring("dbg-cell ".Length);
                        var xy = dbg.Split(',').ToList().ConvertAll(t => int.Parse(t.Trim())).ToArray();
                        if (xy.Length != 2)
                        {
                            Console.WriteLine("Invalid parameters!");
                            break;
                        }

                        var dbgcell = _board.GetCellAtCoordinate(xy[0] - 1, xy[1] - 1);
                        var sb = new StringBuilder();
                        for (var num = 0; num < 9; num++)
                        {
                            sb.Append($"{(num + 1)}: {dbgcell.PossibleNumbers[num]}, ");
                        }

                        var output = sb.ToString();
                        if (output.Length > 0)
                            output = output.Substring(0, output.Length - 2);
                        Console.WriteLine($"Possible numbers for Cell: {output}. Cell set: {dbgcell.Number.HasValue}");
                        Console.WriteLine();
                        break;
                    }

                    case string cmddbg when cmddbg.StartsWith("dbg-sector "):
                    {
                        var dbg1 = cmddbg.Substring("dbg-sector ".Length);
                        var xy12 = dbg1.Split(',').ToList().ConvertAll(t => int.Parse(t.Trim())).ToArray();
                        if (xy12.Length != 3)
                        {
                            Console.WriteLine("Invalid parameters!");
                            break;
                        }

                        var dbgsec = _board.Sectors[xy12[0] - 1, xy12[1] - 1];
                        Console.WriteLine(dbgsec.CountNumberOfPossibleCellsForNumber(xy12[2]));
                        break;
                    }
                    
                    case string cmddbg when cmddbg.StartsWith("dbg-all"):
                    {
                        var dbg1 = cmddbg.Substring("dbg-all".Length);

                        for (var y = 0; y < 9; y++)
                        {
                            for (var x = 0; x < 9; x++)
                            {
                                var dbgcell = _board.GetCellAtCoordinate(x, y);
                                var sb = new StringBuilder();
                                for (var num = 0; num < 9; num++)
                                {
                                    sb.Append($"{(num + 1)}: {dbgcell.PossibleNumbers[num]}, ");
                                }

                                var output = sb.ToString();
                                if (output.Length > 0)
                                    output = output.Substring(0, output.Length - 2);
                                Console.WriteLine($"Cell ({x + 1}, {y + 1}): {output}. Cell set: {dbgcell.Number.HasValue}");

                            }
                        }

                        break;
                    }

                    case string cmdload when cmdload.StartsWith("load "):
                    {
                        var path = cmdload.Substring("load ".Length);
                        var text = File.ReadAllText(path).Replace("\n", "").ToCharArray().ToList().ConvertAll(t =>
                        {
                            if (!int.TryParse(t.ToString(), out var b))
                            {
                                return (int?) null;
                            }

                            return (int?) b;
                        }).ToArray();
                        for (var y = 0; y < 9; y++)
                        {
                            for (var x = 0; x < 9; x++)
                            {
                                var pos = (y * 9) + x;
                                _board.SetCell(x,y,text[pos]);
                            }
                        }

                        break;
                    }

                    case string cmd when cmd.StartsWith("set "):
                    {
                        var point = cmd.Replace("set ", "").Trim();
                        var xyn = point.Split(',').ToList().ConvertAll(t => int.Parse(t.Trim())).ToArray();
                        if (xyn.Length != 3)
                        {
                            Console.WriteLine("Invalid parameters!");
                            break;
                        }
                        _board.SetCell(xyn[0] - 1, xyn[1] - 1, xyn[2]);
                        break;
                    }
                        
                    
                    default:
                        Console.WriteLine("Invalid Command!");
                        break;
                }
            }

        }

        private static void Solve()
        {
            Console.Write("Working...");
            if (_showAllSteps)
            {
                Console.WriteLine();
            }
            var iterations = 0;
            while (!_board.IsSolved() && iterations < 200)
            {
                _board.Solve();
                if (_showAllSteps)
                {
                    _board.Draw();
                    Console.WriteLine("\n\n");
                }
                iterations++;
            }

            if (!_board.IsSolved())
            {
                Console.WriteLine("Iteration count exceeded. Sudoku is not solvable.");
                return;
            }
            Console.WriteLine(" Done!");
            Console.WriteLine($"Solved in {iterations} iterations!");
        }

        private static string GetInput()
        {
            Console.Write("> ");
            return Console.ReadLine();
        }
    }
}